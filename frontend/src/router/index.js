import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import UserToken from '@/views/user/UserToken.vue'
import AccountOwnedAuctions from '@/views/account/AccountOwnedAuctions.vue'
import AccountWonAuctions from '@/views/account/AccountWonAuctions.vue'
import AccountProfile from '@/views/account/AccountProfile.vue'
import Auction from '@/views/auction/Auction.vue'
import AuctionCreate  from '@/views/auction/AuctionCreate.vue'
import AuctionEdit from '@/views/auction/AuctionEdit'
import AuctioneerPick from '@/views/auctioneer/AuctioneerPick.vue'
import AuctioneerManaged from '@/views/auctioneer/AuctioneerManaged.vue'
import AdminUsers from '@/views/admin/AdminUsers.vue'
import UserProfile from '@/views/user/UserProfile.vue'
import Error500 from '@/views/error/Error500'
import Error400 from '@/views/error/Error400'
import { useAuthStore } from '@/store/auth'

const accountChildrenRoutes = [
  {
    path: 'profile',
    component: AccountProfile,
  },
  {
    path: 'token',
    component: UserToken,
  },
  {
    path: 'owned',
    component: AccountOwnedAuctions,
  },
  {
    path: 'won',
    component: AccountWonAuctions,
  }
];

const auctioneerChildrenRoutes = [
  {
    path: 'pick',
    component: AuctioneerPick,
  },
  {
    path: 'managed',
    component: AuctioneerManaged,
  }
];

const adminChildrenRoutes = [
  {
    path: 'users',
    component: AdminUsers,
  },
];

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/About.vue')
  },
  {
    path: '/account',
    name: 'account',
    children: accountChildrenRoutes,
  },
  {
    path: '/admin',
    name: 'admin',
    children: adminChildrenRoutes,
  },
  {
    path: '/auctioneer',
    name: 'auctioneer',
    children: auctioneerChildrenRoutes,
  },
  {
    path: '/users/:id',
    component: UserProfile,
  },
  {
    path: '/auctions/create',
    component: AuctionCreate,
  },
  {
    path: '/auctions/:id',
    component: Auction
  },
  {
    path: '/auctions/:id/edit',
    component: AuctionEdit,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
});

router.beforeEach((to, from) => {
  const store = useAuthStore();
  if (store.tokenExists('access_token')) {
    const data = store.introspectAccessToken();
    console.log("Access Token validity checked, data: ", data);
    console.log(store.currentUser);
  } else {
    console.log("Access Token is not present, skipping refresh token")
  }
});

export default router
