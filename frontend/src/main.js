/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'

// Components
import App from './App.vue'

// Composables
import { createApp, provide } from 'vue'

const app = createApp(App)

registerPlugins(app)
app.provide('axios', app.config.globalProperties.$axios)
app.mount('#app')
