/**
 * plugins/index.js
 *
 * Automatically included in `./src/main.js`
 */

// Plugins
import vuetify from './vuetify'
import { setUpPinia } from '@/plugins/pinia'
import router from '../router'
import axios from './axios'

export function registerPlugins (app) {
  app.use(router);
  app.use(axios);
  setUpPinia(app);
  app.use(vuetify);
}
