/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://pinia.vuejs.org`
 */
import { createPinia } from 'pinia'


export function setUpPinia (app) {
  const pinia = createPinia()
  pinia.use(({ store }) => {
    store.$axios = app.config.globalProperties.$axios;
  });
  app.use(pinia)
}
