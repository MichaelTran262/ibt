/**
 * plugins/axios.js
 *
 * Framework documentation: https://pinia.vuejs.org`
 */
import axios from 'axios'
import createAuthRefreshInterceptor from 'axios-auth-refresh';

// https://github.com/Flyrell/axios-auth-refresh
const refreshTokenLogic = (failedRequest) => {
  axios.post('/api/token/refresh').then((data) => {
    return Promise.resolve();
  })
  .catch((error) => {
    console.error("Failed to refresh token: ", error);
    return Promise.reject(error);
  });
}

const axiosInstance = axios.create();
createAuthRefreshInterceptor(axiosInstance, refreshTokenLogic);

export default {
  install: (app) => {
    app.config.globalProperties.$axios = { ...axiosInstance };
  }
};
