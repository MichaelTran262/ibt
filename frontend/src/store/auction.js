import { defineStore } from 'pinia';
import { useAuthStore } from './auth'

export const useAuctionStore = defineStore("auction", {
    state: () => ({
      auctions: [],
      totalPages: 0,
      totalItems: 0,
      currentPage: 1,
      auction: {},
      bids: [],
      participants: [],
    }),
    getters: {
      getAuctions: (state) => {
        state.auctions;
      },
      getTotalPages: (state) => {
        state.totalPages;
      },
      getTotalItems: (state) => {
        state.totalItems;
      },
      getCurrentPage: (state) => {
        state.currentPage;
      },
      getParticipants: (state) => {
        state.participants;
      },
      getBids: (state) => {
        state.bids;
      },
      getCurrentAuction: (state) => {
        state.auction;
      },
    },
    actions: {
      async fetchAuction(id) {
        try {
          const response = await this.$axios.get('/api/auctions/' + id);
          this.auction = response.data
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchAuctions(page) {
        console.log('/api/auctions?page=' + page);
        try {
          const response = await this.$axios.get('/api/auctions?page=' + page);
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchMyAuctions(page) {
        try {
          const user = useAuthStore().currentUser;
          const response = await this.$axios.get('/api/users/' + user.userId + '/auctions/owned?page=' + page);
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchWonAuctions(page) {
        try {
          const user = useAuthStore().currentUser;
          const response = await this.$axios.get('/api/users/' + user.userId + '/auctions/won?page=' + page + '&winner_id=' + user['userId']);
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchUserOwnedAuctions(userId) {
        try {
          const response = await this.$axios.get('/api/users/' + userId + '/auctions/owned');
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchUserWonAuctions(userId) {
        try {
          const response = await this.$axios.get('/api/users/' + userId + '/auctions/won');
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchPlannedAuctions(page) {
        try {
          const params = {
            state: 'PLANNED',
          }
          const response = await this.$axios.get('/api/auctions', { params });
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchManagedAuctions(page) {
        try {
          const user = useAuthStore().currentUser;
          const params = {
            auctioneer_id: user['sub'],
          }
          console.log("USER: " + user);
          const response = await this.$axios.get('/api/auctions', { params });
          this.auctions = response.data.items;
          this.totalPages = response.data.total_pages;
          this.totalItems = response.data.total_items;
          this.currentPage = response.data.current_page;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchBids(auctionId) {
        try {
          const params = {
            auction_id: auctionId,
          }
          const response = await this.$axios.get('/api/bids', { params });
          this.bids = response.data.items;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchParticipants(auctionId) {
        try {
          const params = {
            auction_id: auctionId,
          };
          const response = await this.$axios.get('/api/participants', { params });
          this.participants = response.data.items;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async fetchBids(auctionId) {
        try {
          const params = {
            auction_id: auctionId,
          };
          const response = await this.$axios.get('/api/bids', { params });
          this.bids = response.data.items;
        } catch (error) {
          console.error(error);
        }
      },
      async changeAuctionToReady(auctionId) {
        try {
          var putData = { ...this.auction };
          putData.state = 'READY';
          const user = useAuthStore().currentUser;
          putData.auctioneer_id = user['userId'];
          const id = putData.id;
          const response = await this.$axios.put('/api/auctions/' + id, putData);
          this.auction = response.data;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async changeAuctionToOngoing(auctionId) {
        try {
          var putData = { ...this.auction };
          putData.state = 'ONGOING';
          const id = putData.id;
          const response = await this.$axios.put('/api/auctions/' + id, putData);
          this.auction = response.data;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async changeAuctionToEnded(auctionId) {
        try {
          var putData = { ...this.auction };
          putData.state = 'ENDED';
          const id = putData.id;
          console.log(putData);
          const response = await this.$axios.put('/api/auctions/' + id, putData);
          this.auction = response.data;
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async participateAuction(auctionId) {
        try {
          const postData = {
            auction_id: auctionId,
          };
          console.log('Posting data:', postData);
          const response = await this.$axios.post('/api/participants', postData);
          console.log(response.data);
          this.fetchParticipants(auctionId);
          this.fetchBids(auctionId);
        } catch (error) {
          if (error.response.data['message']) {
            alert(error.response.data['message']);
          } else {
            alert(error);
          }
          console.error(error);
        }
      },
      async reset() {
        this.currentPage = 1;
      }
    },
});
