import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
    state: () => ({
        currentUser: null,
        isAuthenticated: false,
        accessToken: null,
        idToken: null,
        refreshToken: null,
    }),
    getters: {
    },
    actions: {
        checkAuth() {
            if (this.tokenExists('id_token')) {
                this.isAuthenticated = true;
                this.idToken = this.getIdToken();
                this.currentUser = this.getUser();
            } else {
                this.isAuthenticated = false;
            }
        },
        //
        tokenExists(type) {
          const cookieValue = document.cookie.split('; ').find((row) => row.startsWith(type));
          if (cookieValue) {
              return true;
          }
          return false;
        },
        parseJwt (token) {
          var base64Url = token.split('.')[1];
          var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
          var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          }).join(''));
          return JSON.parse(jsonPayload);
        },
        // Returns access Token as an object. Will only work if access token is not set with httpOnly attribute
        getAccessToken() {
          if (this.tokenExists('access_token')) {
            const cookieValue = document.cookie.split('; ').find((row) => row.startsWith(`access_token`));
            var token = this.parseJwt(cookieValue);
            return token;
          } else {
            return "No Token! This should never happen."
          }
        },
        // Returns Id Token as an object. Will only work if access token is not set with httpOnly attribute
        getIdToken() {
          if (this.tokenExists('id_token')) {
            const cookieValue = document.cookie.split('; ').find((row) => row.startsWith(`id_token`));
            var token = this.parseJwt(cookieValue);
            return token;
          } else {
            return "No ID Token!"
          }
        },
        // Returns Refresh Token as an object. Will only work if access token is not set with httpOnly attribute
        getRefreshToken() {
          if (this.tokenExists('refresh_token')) {
            const cookieValue = document.cookie.split('; ').find((row) => row.startsWith(`refresh_token`));
            var token = this.parseJwt(cookieValue);
            return token;
          } else {
            return "No refresh Token!"
          }
        },
        getUser() {
          const token = this.getIdToken();
          var user = new Object();
          user['userId'] = token['sub'];
          user['username'] = token['preferred_username'];
          user['email'] = token['email'];
          user['firstname'] = token['given_name'];
          user['lastname'] = token['family_name'];
          user['picture'] = token['picture'];
          user['groups'] = token['groups'];
          return user
        },
        async refreshAccessToken() {
          const response = await this.$axios.get('/api/token/refresh');
        },
        introspectAccessToken() {
          const response = this.$axios.get('/api/token/introspect/access');
          return response.data;
        },
    },
});
