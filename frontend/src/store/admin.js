import { defineStore } from 'pinia';

import axios from 'axios';
import { useAuthStore } from './auth'

export const useAdminStore = defineStore("admin", {
    state: () => ({
      userCount: 0,
    }),
    getters: {
    },
    actions: {
      async getUsers() {
        try {
          const store = useAuthStore();
          if (store.accessToken) {
            const headers = {
              'Authorization': 'Bearer ' + JSON.stringify(store.accessToken),
              'Content-Type': 'application/json'
            };
            const response = await axios.get('/api/admin/users/count', { headers });
            console.log(response);
            this.userCount = Math.random();
            return users;
          } else {
            throw "Missing access token";
          }
        } catch (error) {
          console.error('Error fetching user count:', error);
        }
      },
    },
});
