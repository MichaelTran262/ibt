import { defineStore } from "pinia";

export const useUserStore = defineStore("user", {
    state: () => ({
        userCount: null,
        users: [],
        user: {},
        isLoading: false,
        error: '',
    }),
    getters: {
    },
    actions: {
      async fetchUser(id) {
        try {
          const response = await this.$axios.get('/api/users/' + id);
          this.user = response.data;
          return response.data
        } catch (err) {
          this.error = err.message;
        } finally {
          this.isLoading = false;
        }
      },
    }
});
