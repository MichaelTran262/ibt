# IBT

Tento repozitář obsahuje soubory a zdroje bakalářské práce

## Praktická část
Praktická část je složena z implementace webového informačního systému založeného na architektuře mikroslužeb (microservices). Implementace bude vytvořena na základě již existujícího systému. Informační systém byl původně postaven v předmětu IIS jako monolitická aplikace s úzce svázaným jednotným kódem a jednou SQL databází.  
V novém informačním systému bude brán důraz na autentizaci a autorizaci využívajícího primárně protokolu OPENID Connect. Bude dále brán ohled na práva a role podle typu uživatele a správného využití JWT. 

### Popis informačního systému
\*Převzato z absolvovaného předmětu Informační systémy.

Úkolem je vytvořit informační systém pro pořádání internetových aukcí určených pro nabídku ale i poptávku aktiv, majetku, zboží nebo služeb. Každá aukce (dražba) má nějaké označení, pomocí které ji účastníci budou moci vhodně odlišit, typ aukce (nabídková, poptávková), pravidla aukce (otevřená, uzavřená) a další atributy dané aukce dle příslušného typu a pravidel aukce (např. popis zboží/majetku, fotky, případně požadavky poptávaného zboží/majetku, vyvolávací cena, apod.). Uzavřená aukce umožňuje vložit pouze jednu skrytou nabídku, zatímco otevřená aukce umožňuje provádět úpravy nabídek účastníků (příhozy, snížení ceny za poptávku) v časovém limitu stanoveným danou aukcí. Výše příhozů mohou být omezené (přesná suma, interval, apod.). Po ukončení aukce jsou výsledky viditelné všem zainteresovaným osobám aukce. Uživatelé budou moci dále informační systém použít konkrétně následujícím způsobem:

#### administrátor

  - spravuje uživatele

  - má rovněž práva všech následujících rolí


#### licitátor (ta osoba s kladívkem)

  - schvaluje navrhované aukce (stává se organizátorem aukce)

  - potvrzuje registrace účastníků

  - plánuje, spouští, ukončuje, vyhodnocuje aukce a zveřejňuje jejich výsledky

  - má rovněž práva registrovaného uživatele (nicméně nesmí být licitátorem v rámci aukcí, v kterých je autorem nebo účastníkem)


#### registrovaný uživatel

  - zakládá a navrhuje aukce - po schválení se stane autorem aukce

  - nastavuje parametry aukce (typ, pravidla)

  - registruje se do aukcí - po schválení se stane účastníkem aukce

  - sleduje stav jeho registrace

  - sleduje stav aukce, provádí příhozy dle pravidel aukce

  - vidí výsledky aukce po zveřejnění licitátorem

  - může se přihlásit externími účty

#### neregistrovaný

  - vidí dostupné aukce a jejich obsah