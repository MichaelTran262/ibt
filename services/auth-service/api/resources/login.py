from flask import Flask, jsonify, session, render_template, request, redirect, url_for, make_response, Response, abort
from flask_restful import Resource

from api.app import oauth

# Login endpoint
class Login(Resource):
    def get(self):
        redirect_uri = url_for('authorize', _external=True)
        resp = oauth.keycloak.authorize_redirect(redirect_uri)
        return resp 