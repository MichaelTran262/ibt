from flask import redirect, make_response
from flask_restful import Resource

from api.app import oauth, app, HOST_URL

class Authorize(Resource):
    def get(self):
        # This request should always receive session_state, iss, code parameters
        token = oauth.keycloak.authorize_access_token()
        resp = make_response(redirect(HOST_URL))
        #  set httponly=True and samesite='Strict' in production
        resp.set_cookie('access_token', str(token['access_token']))
        resp.set_cookie('id_token', str(token['id_token']))
        resp.set_cookie('refresh_token', str(token['refresh_token']))
        return resp