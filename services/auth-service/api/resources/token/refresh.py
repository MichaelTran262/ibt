from flask import redirect, make_response, request, jsonify, abort
from flask_restful import Resource
from api.app import oauth, client, app, KEYCLOAK_URL

import requests

# Refresh token endpoint
class Refresh(Resource):
    def post(self):
        refresh_token = request.cookies.get('refresh_token')
        if refresh_token is None:
            app.logger.warning("No refresh token! Aborting request.")
            abort(401)
        url = KEYCLOAK_URL + '/realms/vutbids/protocol/openid-connect/token'
        try:
            token = client.refresh_token(url, refresh_token=refresh_token)
            resp = make_response(jsonify({"message": "New access token retrieved."}), 200)
            resp.set_cookie('access_token', str(token['access_token']))
            return resp
        except requests.exceptions.RequestException as e:
            return jsonify({"msg": "error happened " + str(e)})