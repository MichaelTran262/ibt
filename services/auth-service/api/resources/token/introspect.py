from flask import request, jsonify
from flask_restful import Resource

from api.common.decorators import token_required
from api.app import client, KEYCLOAK_URL, app

import json
import requests

class Introspect(Resource):
    @token_required
    def get(self):
        url = KEYCLOAK_URL + '/realms/vutbids/protocol/openid-connect/token/introspect'
        access_token = request.cookies.get('access_token')
        try:
            resp = client.introspect_token(url=url, token=access_token, token_type_hint="access_token")
            json_data = json.loads(resp.text)
            if json_data['active'] is True:
                return jsonify({'token_active': True})
            else:
                return jsonify({'token_active': False})
        except requests.exceptions.RequestException as e:
            app.logger.error(e)
            return jsonify({"msg": "error happened " + str(e)})
