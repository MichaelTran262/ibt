from flask import jsonify, request, redirect, make_response
from flask_restful import Resource

from api.app import CLIENT_ID, KEYCLOAK_URL, HOST_URL, app

import requests


# Login endpoint
class Logout(Resource):
    def get(self):
        # Remove Keycloak session
        url = KEYCLOAK_URL + '/realms/vutbids/protocol/openid-connect/logout'
        token = request.cookies.get('access_token')
        refresh_token = request.cookies.get('refresh_token')
        id_token = request.cookies.get('id_token')
        #decoded = decode_token(token)
        headers = {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + str(token) + 'a'
        }
        obj = {'client_id': CLIENT_ID,
               'refresh_token': refresh_token,
               'post_logout_redirect_uri': HOST_URL,
               'id_token_hint': id_token
               }
        try:
            r = requests.get(url, headers=headers, params=obj)
            app.logger.debug(r.text)
            if r.status_code >= 400:
                data = r.content
                flask_resp = make_response(data, r.status_code)
                return flask_resp
            flask_resp = make_response(redirect(HOST_URL + '/'))
            flask_resp.delete_cookie('access_token')
            flask_resp.delete_cookie('token')
            flask_resp.delete_cookie('id_token')
            return flask_resp
        except requests.exceptions.RequestException as e:
            app.logger.error(e)
            return jsonify({"msg": "error happened " + str(e)})