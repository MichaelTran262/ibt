from flask import request
from flask_restful import Resource

from api.app import CLIENT_ID, KEYCLOAK_URL

import requests


class LinkedAccounts(Resource):
    def get(self):
        token = request.cookies.get('access_token')
        url = KEYCLOAK_URL + "/realms/vutbids/account/linked-accounts"
        headers = {
                'content-type': 'application/json',
                'Authorization' : 'Bearer ' + str(token)
                }
        x = requests.get(url, headers=headers)
        return x.text
