from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from authlib.integrations.flask_client import OAuth
from authlib.integrations.requests_client import OAuth2Session
from dotenv import load_dotenv
from api import set_env_variables

import os

load_dotenv()

# Load external config

app =  Flask(__name__)

api = Api(app)

if os.getenv('CONFIGSERVER_URL'):
    set_env_variables(os.getenv('CONFIGSERVER_URL'))
else:
    app.logger.error("Missing CONFIGSERVER_URL. Exiting...")
    exit(1)

app.secret_key = os.getenv('SECRET_KEY')

CORS(app)
oauth = OAuth(app)
KEYCLOAK_URL = os.getenv('KEYCLOAK_URL')
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
PUBKEY = '-----BEGIN PUBLIC KEY-----\n' + os.getenv('PUBKEY') + '\n-----END PUBLIC KEY-----'
HOST_URL = os.getenv('HOST_URL')
SCOPE = 'openid email profile'
oauth.register('keycloak', 
               client_id=CLIENT_ID,
               client_secret=CLIENT_SECRET,
               access_token_url=KEYCLOAK_URL + 
               "/realms/vutbids/protocol/openid-connect/token",
               authorize_url=KEYCLOAK_URL + "/realms/vutbids/protocol/openid-connect/auth",
               server_metadata_url=KEYCLOAK_URL+ "/realms/vutbids/.well-known/openid-configuration",
               client_kwargs={
                    'scope': SCOPE
                },
            )

client = OAuth2Session(client_id=oauth.keycloak.client_id,
                       client_secret=oauth.keycloak.client_secret,
                       scope=oauth.keycloak.client_kwargs['scope'])


from api.resources.login import Login
from api.resources.authorize import Authorize
from api.resources.logout import Logout
from api.resources.account.linked import LinkedAccounts
from api.resources.token.introspect import Introspect
from api.resources.token.refresh import Refresh

def parse_and_print_resp(response):
    http_string = f"{response.status} {response.status_code}\r\n"
    for header, value in response.headers:
        http_string += f"{header}: {value}\r\n"
    http_string += "\r\n"
    http_string += response.get_data(as_text=True)

    return http_string


api.add_resource(Login, '/login')
api.add_resource(Logout, '/logout')
api.add_resource(Authorize, '/authorize')
api.add_resource(LinkedAccounts, '/api/account/linked')
api.add_resource(Refresh, '/api/token/refresh')
api.add_resource(Introspect, '/api/token/introspect/access')

if __name__ == "__main__":
    app.run(port=5000, debug=True)