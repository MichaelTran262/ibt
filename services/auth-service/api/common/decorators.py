from flask import request, abort, g
from flask import current_app as app
from functools import wraps
from api.app import PUBKEY
import jwt


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # Check if Authorization header is present
        if 'Authorization' in request.headers:
            # Extract token from Authorization header
            token = request.headers['Authorization'].split()[1]
            token = str.replace(str(token), 'Bearer ', '')
        else:
            token = request.cookies.get('access_token')
        if not token:
            return {'message': 'Token is missing'}, 401
        try:
            key_binary = PUBKEY.encode('ascii')
            decoded = jwt.decode(token, key_binary, audience=["account"], algorithms=["RS256"])
            app.logger.debug("Token is valid")
            g.decoded_token = decoded
        except jwt.ExpiredSignatureError as e:
            app.logger.error(e)
            app.logger.error("Token expired")
            abort(401)
        except jwt.InvalidTokenError as e:
            app.logger.error(e)
            app.logger.error("Invalid Token")
            abort(401)
        return f(*args, **kwargs)
    return decorated
