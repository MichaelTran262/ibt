from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from authlib.integrations.flask_client import OAuth
from authlib.integrations.requests_client import OAuth2Session
from dotenv import load_dotenv
from api import set_env_variables
import os

load_dotenv()

app =  Flask(__name__)

api = Api(app)

if os.getenv('CONFIGSERVER_URL'):
    set_env_variables(os.getenv('CONFIGSERVER_URL'))
else:
    app.logger.error("Missing CONFIGSERVER_URL. Exiting...")
    exit(1)

app.secret_key = os.getenv('SECRET_KEY')
CORS(app)
oauth = OAuth(app)
KEYCLOAK_URL = os.getenv('KEYCLOAK_URL')
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
PUBKEY = '-----BEGIN PUBLIC KEY-----\n' + os.getenv('PUBKEY') + '\n-----END PUBLIC KEY-----'
HOST_URL = os.getenv('HOST_URL')
SCOPE = 'openid email profile'
oauth.register('keycloak', 
               client_id=CLIENT_ID,
               client_secret=CLIENT_SECRET,
               access_token_url=KEYCLOAK_URL + 
               "/realms/vutbids/protocol/openid-connect/token",
               authorize_url=KEYCLOAK_URL + "/realms/vutbids/protocol/openid-connect/auth",
               server_metadata_url=KEYCLOAK_URL+ "/realms/vutbids/.well-known/openid-configuration",
               client_kwargs={
                    'scope': SCOPE
                },
            )

def parse_and_print_resp(response):
    http_string = f"{response.status} {response.status_code}\r\n"
    for header, value in response.headers:
        http_string += f"{header}: {value}\r\n"
    http_string += "\r\n"
    http_string += response.get_data(as_text=True)

    return http_string

from api.resources.user import User
from api.resources.auctions.user_auction_owned import UserAuctionOwned
from api.resources.auctions.user_auction_won import UserAuctionWon


api.add_resource(User, '/api/users/<id>')
api.add_resource(UserAuctionOwned, '/api/users/<id>/auctions/owned')
api.add_resource(UserAuctionWon, '/api/users/<id>/auctions/won')

if __name__ == "__main__":
    app.run(port=5000, debug=True)