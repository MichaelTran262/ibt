import os
import requests


def set_env_variables(url):
    try:
        resp = requests.get(url)
        resp.raise_for_status()

        config = resp.json()

        for key, value in config.items():
            os.environ[key] = value
    except requests.exceptions.RequestException as e:
        print(e)
        print("Something went wrong with request for config from configserver. Exiting application")
        exit(1)