from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource
from api.common.helpers import fetch_token_from_keycloak
from api.common.decorators import token_required
from api.app import HOST_URL

import requests

class UserAuctionOwned(Resource):
    def get(self, id):
        token = fetch_token_from_keycloak()
        headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json',
        }
        params = {
            'author_id': str(id)
        }
        url = HOST_URL + '/api/auctions'
        try: 
            resp = requests.get(url, headers=headers, params=params)
            resp_data = resp.json()
            app.logger.debug(resp_data)
            return resp_data
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving user!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving user, reason: ' + e.response.text}
    