from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource
from api.common.helpers import get_user_group, get_token, fetch_token_from_keycloak
from api.common.decorators import token_required
from api.app import KEYCLOAK_URL

import requests
import os

class User(Resource):
    def get(self, id):
        token = fetch_token_from_keycloak()
        headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        url = KEYCLOAK_URL + '/admin/realms/vutbids/users/' + str(id)
        try: 
            resp = requests.get(url, headers=headers)
            resp_data = resp.json()
            app.logger.debug(resp_data)
            return resp_data
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving user!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving user, reason: ' + e.response.text}