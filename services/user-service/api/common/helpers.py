from flask import request, abort
from api.app import CLIENT_ID, CLIENT_SECRET, KEYCLOAK_URL, app

import jwt
import base64
import requests


def get_token():
    if 'Authorization' in request.headers:
        # Extract token from Authorization header
        token = request.headers['Authorization'].split()[1]
        token = str.replace(str(token), 'Bearer ', '')
    else:
        token = request.cookies.get['access_token']
        if token is None:
            abort(401)
    return token


def fetch_token_from_keycloak():
    encoded = base64.b64encode((CLIENT_ID + ':' + CLIENT_SECRET).encode()).decode()
    headers = {
        'Authorization': 'Basic ' + encoded,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    url = KEYCLOAK_URL + '/realms/vutbids/protocol/openid-connect/token'
    try:
        resp = requests.post(url, data={
            'grant_type': 'client_credentials'
        }, headers=headers)
        resp_data = resp.json()
        return resp_data['access_token']
    except requests.exceptions.RequestException as e:
        app.logger.error('Error retrieving token!')
        app.logger.error(e.response.text)
        return {'msg': 'Error retrieving token, reason: ' + e.response.text}


def get_user_group(token):
    try:
        PUBKEY = '-----BEGIN PUBLIC KEY-----\n' + os.getenv('PUBKEY') + '\n-----END PUBLIC KEY-----'
        key_binary = PUBKEY.encode('ascii')
        decoded = jwt.decode(token, key_binary, audience=["account"], algorithms=["RS256"])
        return decoded
    except jwt.ExpiredSignatureError as e:
        print(e)
        print("Token expired")
        abort(401)
    except jwt.InvalidTokenError as e:
        print(e)
        print("Invalid Token")
        abort(401)
