# Bid service

Tato služba vykonává úkony souvisejících s příhozy.
    
## Jak rozjet

Je potřeba mít nainstalovaný docker, minikube a kubectl.

```
minikube start
eval $(minikube docker-env)
docker build -t vutbids/bid-service .
kubectl apply -k kubernetes
```