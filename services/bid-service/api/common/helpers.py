from flask import request, abort
import jwt
from api.app import PUBKEY
import os


def get_token():
    print(request.headers)
    if 'Authorization' in request.headers:
        # Extract token from Authorization header
        token = request.headers['Authorization'].split()[1]
        token = str.replace(str(token), 'Bearer ', '')
    else:
        token = request.cookies.get['access_token']
        if token is None:
            abort(401)
    return token

def get_user_group(token):
    try:
        key_binary = PUBKEY.encode('ascii')
        decoded = jwt.decode(token, key_binary, audience=["account"], algorithms=["RS256"])
        return decoded
    except jwt.ExpiredSignatureError as e:
        print(e)
        print("Token expired")
        abort(401)
    except jwt.InvalidTokenError as e:
        print(e)
        print("Invalid Token")
        abort(401)
