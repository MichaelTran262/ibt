from flask import g, jsonify, request, make_response
from flask import current_app as app
from flask_restful import Resource
from api.common.decorators import token_required
from api.app import db, HOST_URL
from api.models import BidsDb

import requests

class BidsList(Resource):
    def get(self):
        if 'auction_id' in request.args and 'participant_id' in request.args:
            return make_response(jsonify({'message': 'Both parameters cannot be present simultaneously'}), 400)
        elif 'auction_id' not in request.args and 'participant_id' not in request.args:
            return make_response(jsonify({'message': 'At least one of auction_id or participant_id parameter must be present'}), 400)
        
        bids = None
        if 'auction_id' in request.args:
            app.logger.debug('Received auction_id')
            auction_id = request.args.get('auction_id')
            bids = BidsDb.get_bids_by_auction_id(auction_id=auction_id)
        elif 'participant_id' in request.args:
            participant_id = request.args.get('participant_id')
            bids = BidsDb.get_bids_by_participant_id(participant_id=participant_id)
        response_dict = {
            'items': [bid.to_dict() for bid in bids]
        }
        return make_response(jsonify(response_dict), 200)

    @token_required
    def post(self):
        data = request.get_json()
        participant_id = g.get('decoded_token')['sub']
        if data:
            if not data['auction_id']:
                return make_response(jsonify({'message': 'Must include auction id.'}), 400)
            try:
                url = HOST_URL + '/api/auctions/' + data['auction_id']
                resp = requests.get(url)
                resp_data = resp.json()
                app.logger.debug(resp_data['state'])
                if resp_data['state'] != 'ONGOING':
                    return make_response(jsonify({"message": "Can create bids only in state ONGOING."}), 400)
            except Exception as e:
                app.logger.error('Could not fetch auction from auction-service, check logs of auction service.')
                app.logger.error(e)
                return make_response(jsonify({"message": "Could not fetch auction from auction-service, check logs of auction service."}), 400)
            try:
                db.session.add(BidsDb(
                    participant_id, 
                    data['auction_id'],
                    data['amount']))
                db.session.commit()
                return {"message": f"Bid created."}
            except Exception as e:
                app.logger.error('Something went wrong during POST request, check logs of bid service.')
                app.logger.error(e)
                return make_response(jsonify({"message": "Something went wrong during POST request, check logs of bid service."}), 400)
        else:
            return {"message": "Error, missing data"}
