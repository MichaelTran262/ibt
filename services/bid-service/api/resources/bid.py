from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource
from api.common.decorators import token_required
from api.app import db
from api.models import BidsDb


class Bids(Resource):
    def get(self, id):
        participant = BidsDb.get_bid_by_id(id)
        return jsonify(participant.to_dict())
