from datetime import datetime
from api.app import db, app, HOST_URL

import requests

class BidsDb(db.Model):

    __tablename__ = 'bid'

    id = db.Column(db.Integer, primary_key=True)
    participant_id = db.Column(db.String(36), nullable=False)
    auction_id = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, default=None, nullable=False)
    amount = db.Column(db.Float, default=None, nullable=False)

    def __init__(self, participant_id, auction_id, amount):
        self.participant_id = participant_id
        self.auction_id = auction_id
        self.amount = amount
        self.created = datetime.now()

    def __repr__(self):
        return f"BidsDb(\
            id={self.id}, \
            participant_id={self.participant_id} \
            auction_id={self.auction_id}    \
            amount={self.amount}   " 

    def to_dict(self):
        url = HOST_URL + '/api/users/' + str(self.participant_id)
        participant_username = None
        headers = {
            'Content-Type': 'application/json'
        }
        try: 
            app.logger.debug(url)
            resp = requests.get(url, headers=headers)
            resp_data = resp.json()
            app.logger.debug(resp_data.keys())
            participant_username = resp_data['username']
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving user from user-service!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving user from user-service, reason: ' + e.response.text}
        return {
            'id': self.id,
            'participant_id': self.participant_id,
            'participant_username': participant_username,
            'auction_id': self.auction_id,
            'amount': self.amount,
            'created': self.created
        }

    @staticmethod
    def get_bids_by_auction_id(auction_id, page = 1):
        bids = None
        query = BidsDb.query.order_by(BidsDb.created.desc())
        query = query.filter_by(auction_id=auction_id)
        bids = query.all()
        return bids
    
    @staticmethod
    def get_bids_by_participant_id(participant_id, page = 1):
        bids = None
        query = BidsDb.query.order_by(BidsDb.created.desc())
        query = query.filter_by(participant_id=participant_id)
        bids = query.all()
        return bids

    @staticmethod
    def get_bid_by_id(id):
        bid = BidsDb.query.get(id)
        return bid