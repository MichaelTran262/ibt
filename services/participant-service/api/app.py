from flask import Flask, request, g, abort, jsonify
from flask_restful import Api
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from datetime import datetime
from api import set_env_variables

import enum
import jwt
import os

load_dotenv()


app = Flask(__name__)

api = Api(app)

if os.getenv('CONFIGSERVER_URL'):
    set_env_variables(os.getenv('CONFIGSERVER_URL'))
else:
    app.logger.error("Missing CONFIGSERVER_URL. Exiting...")
    exit(1)
    
app.secret_key = os.getenv('SECRET_KEY')
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv('SQLALCHEMY_DATABASE_URI')
db = SQLAlchemy()
db.init_app(app)
PUBKEY = '-----BEGIN PUBLIC KEY-----\n' + os.getenv('PUBKEY') + '\n-----END PUBLIC KEY-----'
HOST_URL = os.getenv('HOST_URL')
    
from api.resources.participant_list import ParticipantList
from api.resources.participant import Participant

api.add_resource(ParticipantList, '/api/participants')
api.add_resource(Participant, '/api/participants/<id>')
