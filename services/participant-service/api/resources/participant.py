from flask import g, jsonify, request, make_response
from flask import current_app as app
from flask_restful import Resource
from api.common.helpers import get_user_group, get_token
from api.common.decorators import token_required
from api.app import db
from api.models import ParticipantDb


class Participant(Resource):
    def get(self, id):
        participant = ParticipantDb.get_participant_by_id(id)
        return jsonify(participant.to_dict())
    
    @token_required
    def put(self, id):
        user_id = g.get('decoded_token')['sub']
        participant = ParticipantDb.query.get(id)
        data = request.get_json()

        if 'approved' in request.json:
            app.logger.debug(request.json.get('approved'))
            participant.approved = request.json.get('approved')
        else:
            return make_response(jsonify({'message': 'Can only approve participant '}), 400)

        db.session.commit()
        return make_response(jsonify(participant.to_dict()), 200)
    