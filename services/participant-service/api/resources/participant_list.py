from flask import g, jsonify, request, make_response
from flask import current_app as app
from flask_restful import Resource
from api.common.helpers import get_user_group, get_token
from api.common.decorators import token_required
from api.app import db, HOST_URL
from api.models import ParticipantDb

import requests

class ParticipantList(Resource):
    def get(self):
        if 'auction_id' in request.args and 'participant_id' in request.args:
            return make_response(jsonify({'message': 'Both parameters cannot be present simultaneously'}), 400)
        elif 'auction_id' not in request.args and 'participant_id' not in request.args:
            return make_response(jsonify({'message': 'At least one of auction_id or participant_id parameter must be present'}), 400)
        
        participants = None
        if 'auction_id' in request.args:
            app.logger.debug('Received auction_id')
            auction_id = request.args.get('auction_id')
            participants = ParticipantDb.get_participants_by_auction_id(auction_id=auction_id)
        else:
            participant_id = request.args.get('participant_id')
            participants = ParticipantDb.get_participants_by_participant_id(participant_id=participant_id)
        response_dict = {
            'items': [part.to_dict() for part in participants.items],
            'total_pages': participants.pages,
            'total_items': participants.total,
            'current_page': participants.page,
            'has_prev': participants.has_prev,
            'has_next': participants.has_next
        }
        return make_response(jsonify(response_dict), 200)

    @token_required
    def post(self):
        data = request.get_json()
        participant_id = g.get('decoded_token')['sub']
        if data:
            if not data['auction_id']:
                return make_response(jsonify({'message': 'Must include auction id.'}), 400)
            try:
                exists = ParticipantDb.check_participant(participant_id, data['auction_id'])
                if exists:
                    app.logger.debug(exists)
                    return make_response(jsonify({'message': 'Uživatel je již účastníkem aukce.'}), 400)
                db.session.add(ParticipantDb(
                    participant_id=participant_id,
                    auction_id=data['auction_id']
                ))
                db.session.commit()
                return make_response(jsonify({'message': f"Participant {participant_id} joined auction {data['auction_id']} successfully."}), 200)
            except Exception as e:
                app.logger.error('Something went wrong during POST request, check logs of participant service.')
                app.logger.error(e)
                return make_response(jsonify({"message": "Something went wrong during POST request, check logs of participant service."}), 400)
        else:
            return jsonify({"error": "Missing data."})

