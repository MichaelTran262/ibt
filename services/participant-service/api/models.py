from datetime import datetime
from api.app import db, app, HOST_URL

import requests

class ParticipantDb(db.Model):

    __tablename__ = 'participant'

    id = db.Column(db.Integer, primary_key=True)
    participant_id = db.Column(db.String(36), nullable=False)
    auction_id = db.Column(db.Integer, nullable=False)
    joined = db.Column(db.DateTime, default=None, nullable=False)
    approved = db.Column(db.Boolean, default=None, nullable=False)

    def __init__(self, participant_id, auction_id):
        self.participant_id = participant_id
        self.auction_id = auction_id
        self.joined = datetime.now()
        self.approved = False

    def __repr__(self):
        return f"Process(\
            id={self.id}, \
            participant_id={self.participant_id} \
            auction_id={self.auction_id} \
            joined={self.joined} \
            approved={self.approved}"
    
    def to_dict(self):
        url = HOST_URL + '/api/users/' + str(self.participant_id)
        participant_username = None
        headers = {
            'Content-Type': 'application/json'
        }
        try: 
            app.logger.debug(url)
            resp = requests.get(url, headers=headers)
            resp_data = resp.json()
            app.logger.debug(resp_data.keys())
            participant_username = resp_data['username']
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving user from user-service!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving user from user-service, reason: ' + e.response.text}
        return {
            'id': self.id,
            'participant_id': self.participant_id,
            'participant_username': participant_username,
            'auction_id': self.auction_id,
            'joined': self.joined,
            'approved': self.approved
        }

    @staticmethod
    def get_participants_by_auction_id(auction_id, page = 1):
        participants = None
        query = ParticipantDb.query.order_by(ParticipantDb.joined.desc())
        query = query.filter_by(auction_id=auction_id)
        participants = query.paginate(page=page, per_page=10)
        return participants
    
    @staticmethod
    def get_participants_by_participant_id(participant_id, page = 1):
        participants = None
        query = ParticipantDb.query.order_by(ParticipantDb.joined.desc())
        query = query.filter_by(participant_id=participant_id)
        participants = query.paginate(page=page, per_page=10)
        return participants
    
    @staticmethod
    def check_participant(participant_id, auction_id):
        exists = ParticipantDb.query.filter_by(participant_id=participant_id, auction_id=auction_id).first() is not None
        return exists

    @staticmethod
    def get_participant_by_id(id):
        auction = ParticipantDb.query.get(id)
        return auction