from flask import Flask, jsonify
from flask_restful import Api, Resource


app = Flask(__name__)
api = Api(app)


class ConfigResource(Resource):
    def get(self):
        config_data = {}
        with open('.env', 'r') as env_file:
            for line in env_file:
                if '=' in line:
                    key, value = line.strip().split('=', 1)
                    config_data[key] = value
        return jsonify(config_data)

api.add_resource(ConfigResource, '/config')

if __name__ == '__main__':
    app.run(debug=True)