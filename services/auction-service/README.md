# Auction service

Tato služba vykonává úkony souvisejících s aukcemi.
    
## Jak rozjet

Je potřeba mít nainstalovaný docker, minikube a kubectl.

```
minikube start
eval $(minikube docker-env)
docker build -t vutbids/auction-service .
kubectl apply -k kubernetes
```