from datetime import datetime
from api.app import db

import enum


class AuctionStateEnum(enum.Enum):
    PLANNED = 'PLANNED'
    READY = 'READY'
    ONGOING = 'ONGOING'
    ENDED = 'ENDED'


class AuctionDb(db.Model):

    __tablename__ = 'auction'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    desc = db.Column(db.String(256), nullable=True)
    picture = db.Column(db.String(256), nullable=True)
    state = db.Column(db.Enum(AuctionStateEnum), nullable=False)
    initial_price = db.Column(db.Float, nullable=False)
    created = db.Column(db.DateTime, default=None, nullable=False)
    date_start = db.Column(db.Date, default=None, nullable=True)
    date_end = db.Column(db.Date, default=None, nullable=True)
    author_id = db.Column(db.String(36), nullable=False)
    auctioneer_id = db.Column(db.String(36), nullable=True)
    winner_id = db.Column(db.String(36), nullable=True)

    def __init__(self, name, author, desc, start_date, end_date, initial_price=0):
        self.name = name
        self.author_id = author
        if not desc:
            self.desc = ""
        else:
            self.desc = desc
        self.created = datetime.now()
        self.date_start = start_date
        self.date_end = end_date
        self.state = AuctionStateEnum.PLANNED
        self.initial_price=initial_price

    def __repr__(self):
        return f"Process(\
            id={self.id}, \
            name={self.name}, \
            desc={self.desc}, \
            state={self.state}, \
            created={self.created}, \
            start_date={str(self.date_start)}, \
            end_date={self.date_end}) \
            author={self.author_id} \
            auctioneer_id={self.auctioneer_id} \
            winner_id={self.winner_id}" 
    
    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.desc,
            'state': str(self.state.value),
            'initial_price': str(self.initial_price),
            'created': self.created.isoformat(),
            'start_date': str(self.date_start),
            'end_date': str(self.date_end),
            'author_id': self.author_id,
            'auctioneer_id': self.auctioneer_id,
            'winner_id': self.winner_id
        }

    @staticmethod
    def get_auctions(page = 1, author_id=None, auctioneer_id=None, winner_id=None, state=None, 
                     start_date=None, end_date=None):
        auctions = None
        query = AuctionDb.query.order_by(AuctionDb.created.desc())
        if author_id is not None:
            query = query.filter_by(author_id=author_id)

        if auctioneer_id is not None:
            query = query.filter_by(auctioneer_id=auctioneer_id)
        
        if state is not None:
            query = query.filter_by(state=state)

        if start_date is not None:
            query = query.filter_by(start_date=start_date)
        
        if end_date is not None:
            query = query.filter_by(end_date=end_date)

        if winner_id is not None:
            query = query.filter_by(winner_id=winner_id )

        auctions = query.paginate(page=page, per_page=10)
        return auctions

    @staticmethod
    def get_auction_by_id(id):
        auction = AuctionDb.query.get(id)
        return auction
    