from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource
from api.app import HOST_URL

import requests

class AuctionParticipants(Resource):
    def get(self, id):
        params = {
            'auction_id': str(id)
        }
        url = HOST_URL + '/api/participants'
        try: 
            resp = requests.get(url, params=params)
            resp_data = resp.json()
            app.logger.debug(resp_data)
            return resp_data
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving participants of auction!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving participants of auction, reason: ' + e.response.text}
    