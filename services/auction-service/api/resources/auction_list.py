from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource
from api.common.decorators import token_required
from api.app import db
from api.models import AuctionDb


class AuctionsList(Resource):
    def get(self):
        args = self.parse_arguments();
        auctions_pagination = AuctionDb.get_auctions(
            args['page'], 
            args['author_id'],
            args['auctioneer_id'],
            args['winner_id'],
            args['state'],
            args['start_date'],
            args['end_date']
        )
        response_dict = {
            'items': [auction.to_dict() for auction in auctions_pagination.items],
            'total_pages': auctions_pagination.pages,
            'total_items': auctions_pagination.total,
            'current_page': auctions_pagination.page,
            'has_prev': auctions_pagination.has_prev,
            'has_next': auctions_pagination.has_next
        }
        return jsonify(response_dict)

    @token_required
    def post(self):
        data = request.get_json()
        print(data)
        owner_id = g.get('decoded_token')['sub']
        if data:
            try:
                db.session.add(AuctionDb(
                    data['name'], 
                    owner_id, 
                    data['desc'], 
                    data['start_date'],
                    data['end_date'],
                    data['initial_price']))
                db.session.commit()
                return {"message": f"Auction {data['name']} created."}
            except Exception as e:
                app.logger.error(e)
        else:
            return {"message": "Error, missing data"}

    def parse_arguments(self):
        # Parsing query parameters
        args = {
            'page': request.args.get('page', 1, type=int),
            'author_id': request.args.get('author_id', type=str),
            'auctioneer_id': request.args.get('auctioneer_id', type=str),
            'winner_id': request.args.get('winner_id', type=str),
            'state': request.args.get('state', type=str),
            'start_date': request.args.get('start_date', type=str),
            'end_date': request.args.get('end_date', type=str)
        }
        app.logger.debug("Received arguments: %s", args)
        return args
