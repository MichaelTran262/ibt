from flask import g, jsonify, request, make_response
from flask import current_app as app
from flask_restful import Resource
from api.common.helpers import get_user_group, get_token
from api.common.decorators import token_required
from api.app import db
from api.models import AuctionDb, AuctionStateEnum


class Auction(Resource):
    def get(self, id):
        auction = AuctionDb.get_auction_by_id(id)
        return jsonify(auction.to_dict())
    
    @token_required
    def put(self, id):
        user_id = g.get('decoded_token')['sub']
        auction = AuctionDb.query.get(id)
        data = request.get_json()

        if auction.auctioneer_id is None and 'auctioneer_id' in request.json and auction.state == AuctionStateEnum.PLANNED:
            auctioneer_id = request.json.get('auctioneer_id')
            if auctioneer_id == auction.author_id: 
                app.logger.error("Nelze být zaroveň licitátorem a autorem aukce!")
                return make_response(jsonify({'message': 'Nelze být zaroveň licitátorem a autorem aukce!'}), 400)
            auction.auctioneer_id = auctioneer_id
            if 'state' in request.json:
                auction.state = request.json.get('state')
            db.session.commit()
            app.logger.info("Auctioneer set successfully.")
            return make_response(jsonify(auction.to_dict()), 200)

        if user_id != auction.author_id and user_id != auction.auctioneer_id:
             return make_response(jsonify({'message': 'You are not author or auctioneer of this auction!'}), 400)

        #if auction.state != AuctionStateEnum.PLANNED:
        #    return jsonify({'message': f'Auction {auction.id} must be in PLANNED state'}), 400
        if not auction:
            return make_response(jsonify({'message': f'Auction with {auction.id} does not exist.'}), 400)

        if 'state' in request.json:
            new_state = request.json.get('state')
            if new_state not in [state.value for state in AuctionStateEnum]:
                return {'message': 'Invalid state value'}, 400
            auction.state = new_state

        if 'start_date' in request.json:
            auction.start_date = request.json.get('start_date')

        if 'end_date' in request.json:
            auction.end_date = request.json.get('end_date')

        if 'author_id' in request.json:
            auction.author_id = request.json.get('author_id')

        if 'auctioneer_id' in request.json:
            auction.auctioneer_id = request.json.get('auctioneer_id')

        if 'winner_id' in request.json:
            app.logger.debug(request.json.get('winner_id'))
            auction.winner_id = request.json.get('winner_id')
        
        db.session.commit()
        return make_response(jsonify(auction.to_dict()), 200)
    