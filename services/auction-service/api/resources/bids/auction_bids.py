from flask import g, jsonify, request
from flask import current_app as app
from flask_restful import Resource

import os
import requests

class AuctionBids(Resource):
    def get(self, id):
        params = {
            'auction_id': str(id)
        }
        url = os.getenv("HOST_URL") + '/api/bids'
        try: 
            resp = requests.get(url, params=params)
            resp_data = resp.json()
            app.logger.debug(resp_data)
            return resp_data
        except requests.exceptions.RequestException as e:
            app.logger.error('Error retrieving bids of auction!')
            app.logger.error(e.response.text)
            return {'msg': 'Error retrieving bids of auction, reason: ' + e.response.text}
    