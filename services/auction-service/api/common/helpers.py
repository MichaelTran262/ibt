from flask import request, abort
from flask import current_app as app
import jwt
import os


def get_token():
    if 'Authorization' in request.headers:
        # Extract token from Authorization header
        token = request.headers['Authorization'].split()[1]
        token = str.replace(str(token), 'Bearer ', '')
    else:
        token = request.cookies.get['access_token']
        if token is None:
            abort(401)
    return token


def get_user_group(token):
    try:
        PUBKEY = os.getenv("PUBKEY")
        key_binary = PUBKEY.encode('ascii')
        decoded = jwt.decode(token, key_binary, audience=["account"], algorithms=["RS256"])
        return decoded
    except jwt.ExpiredSignatureError as e:
        app.logger.error(e)
        app.logger.error("Token expired")
        abort(401)
    except jwt.InvalidTokenError as e:
        app.logger.error(e)
        app.logger.error("Token expired")
        abort(401)
