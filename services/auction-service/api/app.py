from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from api import set_env_variables

import os

load_dotenv()

app = Flask(__name__)
api = Api(app)

if os.getenv('CONFIGSERVER_URL'):
    set_env_variables(os.getenv('CONFIGSERVER_URL'))
else:
    app.logger.error("Missing CONFIGSERVER_URL. Exiting...")
    exit(1)

app.secret_key = os.getenv('SECRET_KEY')
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv('SQLALCHEMY_DATABASE_URI')
db = SQLAlchemy()
db.init_app(app)
PUBKEY = '-----BEGIN PUBLIC KEY-----\n' + os.getenv('PUBKEY') + '\n-----END PUBLIC KEY-----'

from api.resources.auction import Auction
from api.resources.auction_list import AuctionsList

api.add_resource(AuctionsList, '/api/auctions')
api.add_resource(Auction, '/api/auctions/<id>')
