.PHONY: all

images:
	docker build services/auction-service -t auction-service:latest

start:
	minikube start --cpus 8 --memory 8192

build-frontend:
	docker build -t vutbids/frontend frontend
	docker rm nginx-tmp
	docker run --name nginx-tmp -p 8080:80 vutbids/frontend

run-keycloak:
	kubectl apply -f services/namespace.yml
	kubectl apply -k services/keycloak/kubernetes/

run-kong:
	kubectl apply -k services/kong-gateway/kubernetes/

del-kong:
	kubectl delete -k services/kong-gateway/kubernetes/

stop-keycloak:
	kubectl delete all --all -n vutbids-keycloak

run-services:
	kubectl apply -k services/auction-service/kubernetes/

del-services:
	kubectl delete -k services/auction-service/kubernetes/

run-auth:
	kubectl apply -k services/auth-service/kubernetes

del-auth:
	kubectl delete -k services/auth-service/kubernetes

run-client:
	kubectl apply -k frontend/

del:
	kubectl delete all --all -n vutbids
	kubectl delete all --all -n keycloak-vutbids

stop:
	minikube stop

